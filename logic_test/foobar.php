<?php
/*
2. Logic Test
Create a PHP script that is executed form the command line. The script should:
• Output the numbers from 1 to 100
• Where the number is divisible by three (3) output the word “foo”
• Where the number is divisible by five (5) output the word “bar”
• Where the number is divisible by three (3) and (5) output the word “foobar”
• Only be a single PHP file
2.1 Example
An example output of the script would look like:
1, 2, foo, 4, bar, foo, 7, 8, foo, bar, 11, foo, 13, 14, foobar … 
*/

// Divison can have more values in future.
//lets see we want to add 7 in future as 3,5,7, => foo,bar, alpha, can do easily without changing the code.

$divisor = [
    3 => 'foo',
    5 => 'bar'
];
for ($i = 1; $i <= 100; $i++) {
    // set display blank and assume numerical value would be displayed at the end for each number
    $options = [
        'display' => "",       // This will be displayed to user, if it is divisible
        'numeric' => true      // This would check if the value is still numeric or foo/bar
    ];
    $options = checkDivisible($divisor, $i, $options);
    // will print either numeric value or foo , bar depending what is the value.
    echo ($options['numeric'] ? $i : $options['display']) . ($i !== 100 ? ',' : '.') . '  ';
}

/**
 * This function will check both 3 and 5 and return what should be displayed accordingly
 * @param int $divisor
 * @param int $dividend
 * @param array $options
 * @return array $options
 */
function checkDivisible($divisor, $dividend, $options)
{
    foreach ($divisor as $key => $value) {
        if ($dividend % $key == 0) {
            $options['display'] .= $value;
            $options['numeric'] = false;
        }
    }
    return $options;
}
