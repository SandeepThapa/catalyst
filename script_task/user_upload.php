<?php
/**
 * Catalyst Coding Challenge
 * Sandeep Thapa - ersandeepthapa@gmail.com
 * 0424579528
 * Takes database details as commandline options
 **/

if (PHP_SAPI !== 'cli') {
    die('Please run me from Command Line :( ');
}

$arguments = getArguments();

if (array_key_exists('help', $arguments)) {
    displayHelp();
}

$host = $arguments['h'];
$username = $arguments['u'];
$password = $arguments['p'];
$database = "coding_challenge";
$conn = "";

if (!empty($host) && !empty($username)) {
    //gets the database connection to be used in the entire page
    $conn = getDbConnection($host, $username, $password, $database);
    //drop and create create Table if --create_table is supplied
    if (array_key_exists('create_table', $arguments)) {
        dropAndCreateTable($conn);
    }
}
echo "\n";
if (array_key_exists('file', $arguments)) {
    saveRecordsFromFile($conn, $arguments, $argv[0]);
}

function saveRecordsFromFile($conn, $arguments, $path)
{
    $rowsInserted = 0;
    $firstRow = true;
    $file = fopen(dirname(realpath($path)) . '/' . $arguments['file'], "r");
    if (!$file) {
        stdout("Cannot Read the File");
        die();
    }
    //prepare and bind
    $checkTableSql = "show tables like 'users'";
    $tableExists = $conn->query($checkTableSql);
    if ($tableExists->num_rows < 1) {
        //Users table not found, please create one to proceed
        stdout("Users table not found, please create one supplying \"--create_table\" option to proceed");
        die();
    }

    $stmt = $conn->prepare("INSERT INTO users (name, surname, email) VALUES (?, ?, ?)");
    // binding parameters name, surname and email all as string
    $stmt->bind_param("sss", $name, $surname, $email);
    while (!feof($file)) {
        $row = fgetcsv($file);
        if (!empty($row[2]) && !$firstRow) {
            // set parameters and execute
            $name = getCleanName($row[0]);
            $surname = getCleanName($row[1]);
            $email = getCleanEmail($row[2]);
            // check if email address is valid before saving in the dataase
            if (validateEmail($email)) {
                // checking if its dry_run before saving in the database
                if (!array_key_exists('dry_run', $arguments)) {
                    //saving rows to users table in the database
                    if ($stmt->execute()) {
                        $rowsInserted++;
                    }
                    // notify user if email address already exists
                    if ($stmt->errno === 1062) {
                        stdout($email . " is repeated - email address already exists");
                    }
                }
            } else {
                //notify user if email address is not valid
                stdout($email . " is not a valid email address");
            }
        }
        $firstRow = false;
    }
    stdout("\n\n" . $rowsInserted . " rows Inserted Successfully");
    $stmt->close();
    fclose($file);
}

function getArguments()
{
    $db = "u:";    // username
    $db .= "p:";    // password
    $db .= "h:";    // host

    $options = [
        "file:",           // filename
        "create_table",    // create table
        "dry_run",         // Dry run
        "help",            // help
    ];
    return getopt($db, $options);
}

function validateEmail($email)
{
    $user = '[a-zA-Z0-9_\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\']+';
    $domain = '(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.?)+';
    $ipv4 = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
    $ipv6 = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';
    return preg_match("/^$user@($domain|(\[($ipv4|$ipv6)\]))$/", $email);
}

function stdout($msg)
{
    fprintf(STDOUT, $msg . PHP_EOL);
}

function getCleanName($name)
{
    return ucwords($name);
}

function getCleanEmail($email)
{
    $email = preg_replace('/\s+/', '', $email);
    return strtolower($email);
}

/**
 * function to test if any email is repeated,
 * but is not used in the system as it is not checking for repeated emails at the moment.
 * @param $email
 * @param $conn
 * @return bool
 */
function isUniqueEmail($email, $conn)
{
    $stmt = $conn->prepare("SELECT email FROM users WHERE email = ?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    return $stmt->affected_rows > 0 ? false : true;
}

/**
 * Returns the Connection / creates database if not exist
 * @param string $host
 * @param string $username
 * @param string $password
 * @param string $database
 * @return mysqli $connection
 */
function getDbConnection($host, $username, $password, $database)
{
    // Create connection
    $conn = new mysqli($host, $username, $password);

    // Check connection
    if ($conn->connect_error) {
        die('Connection Error (' . $conn->errno . ') ' . $conn->connect_error);
    }

    // create database if not found
    if (!$conn->select_db($database)) {
        $sql = "CREATE DATABASE " . $database;
        if ($conn->query($sql) === true) {
            echo "Database created successfully \n";
        } else {
            die("Error creating database: " . $conn->error);
        }
    }
    return $conn;
}

/**
 * drop and re-create the table
 * @param mysqli $conn
 * @param bool $die
 */
function dropAndCreateTable($conn)
{
    $createTable = 'DROP TABLE IF EXISTS users;';
    $createTable .= "CREATE TABLE users (
                id INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (id),
                name VARCHAR(120) NOT NULL ,
                surname VARCHAR(120) NOT NULL ,
                email VARCHAR(320) NOT NULL ,
                UNIQUE email_index (email)
                )";

    if ($conn->multi_query($createTable) === true) {
        stdout("Table - \"users\" created successfully");
    } else {
        stdout("Error creating table: " . $conn->error);
    }
    die();
}

function displayHelp()
{
    $help = "You can run the Program easily from Command Line. ";
    $help .= "\n Copy the php file into desired folder and supply with all options ";
    $help .= "\n Configure Database Details as \n\t-u username or  -u=username, \n\t-p password or  -p=password, \n\t-h host  or -h=host";
    $help .= "\n Database named coding_challenge will be automatically created if not found";
    $help .= "\n Before you supply CSV file, please create table first  with \n\t --create_table";
    $help .= "\n Then supply file name  as \n\t--file filename.ext or --file=filename.ext – this is the name of the CSV to be parsed";
    $help .= "\n --dry_run – will do all the actions but saving into the database ";
    $help .= "\n --help – will open this message";
    stdout("\n" . $help);
    die();
}

$conn->close();
